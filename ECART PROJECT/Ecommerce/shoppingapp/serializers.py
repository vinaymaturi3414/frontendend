from rest_framework import serializers
from shoppingapp.models import Orders,Orders_items,Products
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username','password','email']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('__all__')


class ProductSerializer(serializers.ModelSerializer):
    image_url= serializers.SerializerMethodField('get_image_url')
    class Meta:
        model = Products
        fields = [
            'id',
            'title',
            'Description',
            'image',
            'price',
            'Created_At',
            'Updated_At',
            'image_url',

        ]

    def get_image_url(self,object):
        return object.image.url

class OrdersItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders_items
        fields = ('__all__')