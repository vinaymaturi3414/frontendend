from shoppingapp.models import Orders,Products,Orders_items
from rest_framework import viewsets,permissions,status
from shoppingapp.serializers import OrdersSerializer,ProductSerializer,UserSerializer,OrdersItemsSerializer
from django.contrib.auth.models import User
from rest_framework.filters import SearchFilter,OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication    
from rest_framework.response import Response
from rest_framework.views import APIView


class OrdersViewset(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    # count=('-products_count')

    def list(self, request):
        # queryset = User.objects.all()
        order_obj =Orders.objects.filter(user_id = request.user)
        serializer = OrdersSerializer(order_obj, many=True)
        return Response(serializer.data)

    def create(self, request,  *args, **kwargs):
        data = request.data
        # userinstance=User.objects.get(id=2)
        print('user:',request.user)
        # print('username:',userinstance)
        new_order = Orders.objects.create(user_id=request.user,total=data['total'],status=data['status'],mode_of_payment=data['mode_of_payment'])
        new_order.save()
        orders_obj = Orders.objects.filter(user_id=request.user)
        print('orders_obj:',orders_obj)
        for product in data['products']:
            product_obj =Products.objects.get(title=product['title'])
            new_order.products.add(product_obj)
        serializer = OrdersSerializer(new_order)

        return Response(serializer.data)

    def retrive(self, request, pk=None):
        queryset = Orders.objects.filter(pk=pk,user_id=request.user, )
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = OrdersSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        pass


    def partial_update(self, request, pk=None):
        orders= Orders.objects.get(user_id=request.user,pk=pk)
        data = request.data

        try:
            for product in data['products']:
                product_obj = Products.objects.get(title=product['title'])
                # products = Products.objects.get(title=data['title'])
                orders.products = product_obj
        except Exception:
            print('error',Exception)

        orders.total =data.get('total',orders.total)
        orders.status = data.get('status', orders.status)
        orders.mode_of_payment = data.get('mode_of_payment', orders.mode_of_payment)
        orders.save()

        serialized = OrdersSerializer(request.user, data=request.data, partial=True)
        return Response(serialized.data,status=status.HTTP_202_ACCEPTED)



class ProductViewSet(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    # permissions_classes=[           
    #     permissions.AllowAny
    # ]
    serializer_class = ProductSerializer
    filter_backends=(SearchFilter,OrderingFilter)
    search_fields=('id','title')

class UserViewSet(viewsets.ModelViewSet):
    queryset=User.objects.all()
    serializer_class= UserSerializer
    
    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (permissions.AllowAny() if self.request.method == 'POST' else IsStaffOrTargetUser()),


    # def perform_update(self, serializer):
    #     password = make_password(self.request.data['password'])
    #     serializer.save(password=password)

class OrdersitemsViewSet(viewsets.ModelViewSet):
    queryset=Orders_items.objects.all()
    # permissions_classes = [
    #     permissions.AllowAny
    # ]
    serializer_class = OrdersItemsSerializer

# class LogoutView(APIView):
#     permission_classes = (IsAuthenticated,)

#     def post(self, request):
#         try:
#             refresh_token = request.data["refresh_token"]
#             token = RefreshToken(refresh_token)
#             token.blacklist()

#             return Response(status=status.HTTP_205_RESET_CONTENT)
#         except Exception as e:
#             return Response(status=status.HTTP_400_BAD_REQUEST)

