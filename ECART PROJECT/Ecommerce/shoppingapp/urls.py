from rest_framework import routers
from .api import OrdersViewset,ProductViewSet,UserViewSet,OrdersitemsViewSet



router=routers.DefaultRouter()
router.register('register',UserViewSet)
router.register('orders',OrdersViewset,)
router.register('Products',ProductViewSet,)
router.register('orderditems',OrdersitemsViewSet,)

urlpatterns =router.urls
