from django.contrib.auth.models import User
from django.db import models
                                                                                                                 
# Create your models here.

class Products(models.Model):
    title = models.CharField(max_length=40)
    Description = models.TextField(max_length = 100)
    image = models.ImageField(upload_to= 'static/')
    price = models.IntegerField()
    Created_At = models.DateTimeField(auto_now_add = True)
    Updated_At = models.DateField(auto_now_add = True)
    def __str__(self):
        return f"{self.title}"



class Orders(models.Model):
    PAYMENT_CHOICES = (
        ("cash", "cash"),
        ("phonpay", "phonepay"),
        ("paytm", "paytm"),
        ("card", "card"),
    )
    STATUS_CHOICES = (
        ("new", "new"),
        ("paid", "paid"),
    )
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    product_id = models.ManyToManyField(Products)
    Total = models.IntegerField()
    Created_At = models.DateTimeField(auto_now_add = True)
    Updated_At = models.DateField(auto_now_add = True)
    status = models.CharField(max_length=20,choices=STATUS_CHOICES)
    mode_of_payment = models.CharField(max_length=20,choices=PAYMENT_CHOICES)
    def __str__(self):
        return f"{self.user_id}"

class Orders_items(models.Model):
    order_id = models.ForeignKey(Orders,on_delete=models.CASCADE)
    product_id = models.ForeignKey(Products,on_delete=models.CASCADE)
    Quantity = models.IntegerField()
    price = models.IntegerField()

    def __str__(self):
        return f"{self.product_id}"
