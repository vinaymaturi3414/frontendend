from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path,include
# from shoppingapp.views import LogoutView
from rest_framework.authtoken.views import obtain_auth_token
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/',obtain_auth_token),
    # path('logout/', LogoutView.as_view(), name='auth_logout'),
    path('',include('shoppingapp.urls')),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
